$compress = @{
  Path = ".\main"
  CompressionLevel = "Optimal"
  DestinationPath = "alblambda.zip"
}
Compress-Archive @compress
