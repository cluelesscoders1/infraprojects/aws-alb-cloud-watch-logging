terraform {
  required_providers {
    aws = {
      source = "hashicorp/aws"
    }
  }
  required_version = ">= 0.13"
}

locals {
  prefix      = "${var.prefix}-${var.region}"
  lambda_hash = filemd5("${path.module}/{var.lambda_payload_name}")
  default_lambda_iam_policies = [
    aws_iam_policy.s3_bucket_access[0].arn,
    aws_iam_policy.lambda_basic_execution[0].arn,
    aws_iam_policy.lambda_cloudwatch[0].arn,
    aws_iam_policy.lambda_kmsRead[0].arn,
  ]
  lambda_iam_policies = var.custom_policies != null ? var.custom_policies : local.default_lambda_iam_policies
  create_iam_policy   = var.custom_policies == null ? 1 : 0
}


module "s3event_lambda" {
  source                   = "./LambdaFunctionDeployment"
  lambda_payload           = "${path.module}/../${var.lambda_payload_name}" # Match with buildandbundle.sh zip file name
  handler_name             = "main"                                         # Name of binary in zip
  lambda_memory            = var.lambda_memory                              # 128MB is the least
  lambda_name              = "${local.prefix}-gos3lambda"
  lambda_runtime           = var.lambda_runtime
  lambda_description       = "S3 Events Lambda function ${local.prefix} ${local.lambda_hash}"
  iam_policy_name          = "${local.prefix}-albGoLambdaS3"
  s3_bucket_id             = var.s3_bucket_id
  lambda_role_arn          = var.lambda_role_arn # Will create a role if null
  lambda_iam_policies      = local.lambda_iam_policies
  cloudwatch_loggroup_name = "${local.prefix}-albS3Event"
  cloudwatch_stream_name   = "${local.prefix}-alb3EventStream"
}


resource "aws_iam_policy" "s3_bucket_access" {
  count = local.create_iam_policy
  name  = "${local.prefix}-alb-GoLambda-s3Readonly"

  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = [
          "s3:Get*",
          "s3:List*",
        ]
        Effect   = "Allow"
        Resource = "*"
      },
    ]
  })
}

resource "aws_iam_policy" "lambda_basic_execution" {
  count = local.create_iam_policy
  name  = "${local.prefix}-alb-GoLambda-basicexecution"

  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = [
          "logs:CreateLogGroup",
          "logs:CreateLogStream",
          "logs:PutLogEvents"
        ]
        Effect   = "Allow"
        Resource = "*"
      },
    ]
  })
}

resource "aws_iam_policy" "lambda_cloudwatch" {
  count = local.create_iam_policy

  name = "${local.prefix}-alb-GoLambda-cloudwatch"

  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = [
          "logs:CreateLogStream",
          "logs:DescribeLogGroups",
          "logs:DescribeLogStreams",
          "logs:GetLogDelivery",
          "logs:DescribeDestinations",
          "logs:GetLogEvents",
          "logs:PutLogEvents",
        ],
        Effect = "Allow"
        "Resource" : "*"
      },
    ]
  })
}


resource "aws_iam_policy" "lambda_kmsRead" {
  count = local.create_iam_policy

  name = "${local.prefix}-alb-GoLambda-kmsRead"

  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = [
          "kms:GetParametersForImport",
        ],
        Effect = "Allow"
        "Resource" : "arn:aws:kms:*"
      },
    ]
  })
}

