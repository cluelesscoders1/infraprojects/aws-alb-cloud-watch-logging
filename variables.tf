variable "prefix" {
  type        = string
  description = "Prefix name for all elements"
  default     = "tst"
}

variable "s3_bucket_id" {
  type        = string
  description = "S3 Bucket Id"
}

variable "lambda_memory" {
  type        = number
  description = "Lambda memory allocation"
  default     = 256
}

variable "region" {
  type        = string
  description = "AWS Region"
}

variable "env" {
  type        = string
  description = "Deployment environment"
}

variable "lambda_runtime" {
  type        = string
  description = "Lambda runtime of deployed function"
  default     = "go1.x"
}

variable "custom_policies" {
  type        = list(string)
  description = "Policy ARNs, null means self generate"
  default     = null
}

variable "lambda_payload_name" {
  type        = string
  description = "Name and path of payload"
  default     = "alblambda.zip"
}

variable "lambda_role_arn" {
  type        = string
  description = "Provided IAM Role ARN"
  default     = null
}

