#!/bin/bash
# Builds go and packages it for pushing to AWS Lambda. 
go mod init main.go
go mod tidy
GOOS=linux CGO_ENABLED=0 go build -o main main.go
zip alblambda.zip main
