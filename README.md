# ALB Logger to Cloud Watch

[[_TOC_]]

# Summary

Parse Application Load Balancer and convert to JSON objets logs in S3 and ship them to Cloud Watch for analysis.

# Architecture

![Lambda Functional Diagram][lambdadiagram]

[lambdadiagram]: alb_lambda_diagram.png "Lambda Functional Diagram"

# Installation

To build, it is assumed the host system has Go installed and the go path setup. For Windows 10 systems, it is assumed 
the user will run Go, Terraform, with access to the AWS client in Git Bash. The PowerShell script provided handles 
ziping up the pacakage were with Linux/Mac can use a single script. 

This scripts assumes an Application Load Balancer was already set up and logging to an S3 bucket already and my not
install properly until the ALB is logging already.

## Linux/Mac
1. Build go with `compileandpackage.sh` or perform the steps manually in the module directy. 
1. Create a sub folder in the module directy, or directory anywhere else in the system.
1. In the folder, create a terraform `main.tf` like shown in the example below.
1. Make sure values in in `main.tf` are as you want. 
1. `terraform init`
1. `terraform apply`

**NOTE:** If the apply fails, make sure the environment is set with the proper AWS account key id and AWS Secret before using terraform.

## Windows

All scripts with `.sh` are assumed to be run in git bash. Scripts with `.ps1` are assumed to be run in PowerShell

1. In the module directory in Git Bash build go with `winbuild.sh` or perform the steps manually. 
1. Run the `zipwin.ps1` script to create a zip file archive to send to AWS.
1. Create a sub folder in the module directy, or directory anywhere else in the system.
1. In the folder, create a terraform `main.tf` like shown in the example below.
1. Make sure values in in `main.tf` are as you want. 
1. `terraform init`
1. `terraform apply`

**NOTE:** If the apply fails, make sure the environment is set with the proper AWS account key id and AWS Secret before using terraform.

## Example Module File

Example assumes being run from a sub folder in the repo not checked in.

```terraform
// Install Lambda for reading the ALB access logs and writing them to CloudWatch

provider "aws" {
  region = "us-east-1"
}

module "alb-s3-lambda-cloudwatch" {
  source = "../"

  lambda_memory       = "256"
  region              = "us-east-1"
  s3_bucket_id        = "test-alb-log-test"
  env                 = "go"
  lambda_payload_name = "alblambda.zip"
}
```

# Resources
* [Example code](https://github.com/aws/aws-lambda-go/blob/master/events/README_S3.md)

# Generated Module Variables

## Modules Called
| Module Name | Source |
| :--- | ---: |
| s3event_lambda | ./LambdaFunctionDeployment |

## Module Resources
| Resource Name |
| :--- | 
| aws_iam_policy |

## Module Variables
| Variable Name | Variable Description | Type | Default |
| :--- | :--- | :---: | ---: |
| prefix | Prefix name for all elements | ${string} | tst |
| s3_bucket_id | S3 Bucket Id | ${string} | None |
| lambda_memory | Lambda memory allocation | ${number} | 256 |
| region | AWS Region | ${string} | None |
| env | Deployment environment | ${string} | None |
| lambda_runtime | Lambda runtime of deployed function | ${string} | go1.x |
| custom_policies | Policy ARNs, null means self generate | ${list(string)} | None |
| lambda_payload_name | Name and path of payload | ${string} | alblambda.zip |
| lambda_role_arn | Provided IAM Role ARN | ${string} | None |

