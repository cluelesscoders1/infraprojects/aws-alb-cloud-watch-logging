variable "iam_policy_name" {
  type        = string
  description = "Desired name associated with Lambda"
  default     = "lambda-ex"
}

variable "lambda_payload" {
  type        = string
  description = "Name of the compressed file payload"
  default     = "archive.zip"
}

variable "lambda_runtime" {
  type        = string
  description = "AWS Lamdba runtime definition"
  default     = "go1.x"
}

variable "lambda_name" {
  type        = string
  description = "Name of AWS Lamda function"
  default     = "TerraLambda"
}

variable "handler_name" {
  type        = string
  description = "Name of the executeable in the zip file"
  default     = "terrahandler"
}

variable "lambda_memory" {
  type        = number
  description = "Memory for lambda function in MB"
  default     = 128
}

variable "lambda_timeout" {
  type        = number
  description = "Timeout in seconds"
  default     = 2
}

variable "lambda_description" {
  type        = string
  description = "Description of lambda function"
  default     = "Default Lambda Description"
}

variable "region" {
  type        = string
  description = "AWS Region"
  default     = "us-east-1"
}

variable "lambda_iam_policies" {
  description = "IAM Policies to be applied"
  default     = []
}

variable "s3_bucket_id" {
  type        = string
  description = "Name of S3 bucket to trigger off of"
}

variable "cloudwatch_loggroup_name" {
  type        = string
  description = "Name for the cloudwatch log group"
  default     = "AlbS3LambdaLogger"
}

variable "cloudwatch_stream_name" {
  type        = string
  description = "Cloudwatch Stream Name"
  default     = "AlbS3LambdaStream"
}

variable "lambda_role_arn" {
  type        = string
  description = "Provided IAM Role ARN"
  default     = null
}
