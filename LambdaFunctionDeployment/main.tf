resource "aws_iam_role" "lambda_user_defined_iam_role" {
  count               = local.function_role_create
  name                = var.iam_policy_name
  managed_policy_arns = var.lambda_iam_policies
  assume_role_policy  = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

locals {
  function_role_arn    = var.lambda_role_arn != null ? var.lambda_role_arn : aws_iam_role.lambda_user_defined_iam_role[0].arn
  function_role_create = var.lambda_role_arn == null ? 1 : 0
}

resource "aws_lambda_function" "lambda_function" {
  filename                       = var.lambda_payload
  function_name                  = var.lambda_name
  role                           = local.function_role_arn
  handler                        = var.handler_name
  description                    = var.lambda_description
  memory_size                    = var.lambda_memory
  timeout                        = 180 # time in seconds
  reserved_concurrent_executions = 1

  # The filebase64sha256() function is available in Terraform 0.11.12 and later
  # For Terraform 0.11.11 and earlier, use the base64sha256() function and the file() function:
  # source_code_hash = "${base64sha256(file("lambda_function_payload.zip"))}"
  source_code_hash = filebase64sha256(var.lambda_payload)

  runtime = var.lambda_runtime

  environment {
    variables = {
      LOG_GROUP  = var.cloudwatch_loggroup_name
      LOG_STREAM = var.cloudwatch_stream_name
    }
  }
}

data "aws_s3_bucket" "s3_trigger_bucket" {
  bucket = var.s3_bucket_id
}

resource "aws_lambda_permission" "allow_bucket" {
  statement_id  = "AllowExecutionFromS3Bucket"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.lambda_function.arn
  principal     = "s3.amazonaws.com"
  source_arn    = data.aws_s3_bucket.s3_trigger_bucket.arn
}

resource "aws_s3_bucket_notification" "s3_lambda_trigger" {
  bucket = data.aws_s3_bucket.s3_trigger_bucket.id
  lambda_function {
    lambda_function_arn = aws_lambda_function.lambda_function.arn
    events              = ["s3:ObjectCreated:*"]
    filter_suffix = ".log.gz"
    id            = "${var.lambda_name}_s3trigger"
  }
  depends_on = [
    aws_lambda_permission.allow_bucket,
  ]
}

resource "aws_cloudwatch_log_group" "alb_loggroup" {
  name              = var.cloudwatch_loggroup_name
  retention_in_days = 5
}

resource "aws_cloudwatch_log_stream" "alb_logstream" {
  name           = var.cloudwatch_stream_name
  log_group_name = aws_cloudwatch_log_group.alb_loggroup.name
}
