# Deploy Lamdba via Terraform 

[[_TOC_]]

# Summary

For deploying lambda functions, terraform has been written to take an archive file and push it to the desired environment.



# Generated Module Variables

## Module Data
| Data Name |
| :--- | 
| aws_s3_bucket |

## Module Resources
| Resource Name |
| :--- | 
| aws_lambda_permission |
| aws_lambda_function |
| aws_iam_role |
| aws_s3_bucket_notification |
| aws_cloudwatch_log_group |
| aws_cloudwatch_log_stream |

## Module Variables
| Variable Name | Variable Description | Type | Default |
| :--- | :--- | :---: | ---: |
| iam_policy_name | Desired name associated with Lambda | ${string} | lambda-ex |
| lambda_payload | Name of the compressed file payload | ${string} | archive.zip |
| lambda_runtime | AWS Lamdba runtime definition | ${string} | go1.x |
| lambda_name | Name of AWS Lamda function | ${string} | TerraLambda |
| handler_name | Name of the executeable in the zip file | ${string} | terrahandler |
| lambda_memory | Memory for lambda function in MB | ${number} | 128 |
| lambda_timeout | Timeout in seconds | ${number} | 2 |
| lambda_description | Description of lambda function | ${string} | Default Lambda Description |
| region | AWS Region | ${string} | us-east-1 |
| lambda_iam_policies | IAM Policies to be applied | N/A | [] |
| s3_bucket_id | Name of S3 bucket to trigger off of | ${string} | None |
| cloudwatch_loggroup_name | Name for the cloudwatch log group | ${string} | AlbS3LambdaLogger |
| cloudwatch_stream_name | Cloudwatch Stream Name | ${string} | AlbS3LambdaStream |
| lambda_role_arn | Provided IAM Role ARN | ${string} | None |

