package main

/*
AWS Lambda Funcion in Golang to work off of an event trigger on a put or push on an S3 bucket.
This lambda is designed to parse the ALB logs so they be organized and push to various targets as
desired later on.

All print statements without further modifications will be viewable in CloudWatch.

Permissions: Needs S3 GetObject permissions for the bucket in questions. Set up all the triggers
and other deployment with the terraform code.
*/

import (
	"bufio"
	"compress/gzip"
	"context"
	"fmt"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/cloudwatchlogs"
	"github.com/aws/aws-sdk-go/service/s3"
	"io/ioutil"
	"os"
	"regexp"
	"strings"
	"sync"
	"time"
	"unsafe"
)

var (
	s3session   *s3.S3
	cloudlogger *cloudwatchlogs.CloudWatchLogs
	// BUCKET_NAME           *string
	LOG_GROUP             *string
	LOG_STREAM            *string
	LOG_STREAM_NEXT_TOKEN *string
	reALB                 *regexp.Regexp
	reUrl                 *regexp.Regexp
)

const (
	// regexpAlb - Regular expression for ALB access log format
	// https://docs.aws.amazon.com/elasticloadbalancing/latest/application/load-balancer-access-logs.html#access-log-entry-format
	regexpAlb = `(?P<type>.*?)\s(?P<timestamp>.*?)\s(?P<elb>.*?)\s(?P<client_port>.*?)\s(?P<target_port>.*?)\s(?P<request_processing_time>.*?)\s(?P<target_processing_time>.*?)\s(?P<response_processing_time>.*?)\s(?P<elb_status_code>.*?)\s(?P<target_status_code>.*?)\s(?P<received_bytes>.*?)\s(?P<send_bytes>.*?)\s"(?P<request>.*?)"\s"(?P<user_agent>.*?)"\s(?P<ssl_cipher>.*?)\s(?P<ssl_procotol>.*?)\s(?P<target_group_arn>.*?)\s"(?P<trace_id>.*?)"\s"(?P<domain_name>.*?)"\s"(?P<chosen_cert_an>.*?)"\s(?P<matched_rule_priority>.*?)\s(?P<request_creation_time>.*?)\s"(?P<actions_executed>.*?)"\s"(?P<redirect_url>.*?)"\s"(?P<error_reason>.*?)"`
	regexpURL = "^https://(.*):[^/]*/([^/]*)/?([^/]*)"

	// regexpURL - Regular expression for service url
	MAX_LINES         = 1024
	MAX_EVENTS_RESULT = 1024
	// The maximum batch size is 1,048,576 bytes. This size is calculated as the sum of all event messages in UTF-8, plus 26 bytes for each log event.
	MAX_BATCH_SIZE  = 1048576
	BYTES_PER_EVENT = 26
	// The maximum number of log events in a batch is 10,000.
	MAX_BATCH_EVENTS = 10000
)


// On init, create global session information.
func init() {
	reALB = regexp.MustCompile(regexpAlb)
	reUrl = regexp.MustCompile(regexpURL)

	lg := os.Getenv("LOG_GROUP")
	ls := os.Getenv("LOG_STREAM")
	REGION := os.Getenv("AWS_REGION")
	LOG_GROUP = &lg
	LOG_STREAM = &ls

	sess := session.Must(session.NewSession(&aws.Config{
		Region: aws.String(REGION),
	}))
	s3session = s3.New(sess)
	cloudlogger = cloudwatchlogs.New(sess)
}

// Groupmap - Mapping group name
func Groupmap(s string, r *regexp.Regexp) map[string]string {
	values := r.FindStringSubmatch(s)
	keys := r.SubexpNames()

	d := make(map[string]string)
	for i := 1; i < len(keys); i++ {
		d[keys[i]] = values[i]
	}
	return d
}

// Tag - Return tag string
func Tag(k string, v string) string {
	return fmt.Sprintf("%s:%s", k, v)
}

// Get the S3 object and save it to disk
func getObject(filename string, bucketName string) {
	fmt.Println("Downloading: ", filename)

	resp, err := s3session.GetObject(&s3.GetObjectInput{
		Bucket: aws.String(bucketName),
		Key:    aws.String(filename),
	})

	if err != nil {
		panic(err)
	}

	// Write file
	body, err := ioutil.ReadAll(resp.Body)
	err = ioutil.WriteFile(filename, body, 0644)
	if err != nil {
		panic(err)
	}
}

// Read Log Files in with the GZ zip.
func ReadLogGz(filename string) ([]byte, error) {
	fileobj, err := os.Open(filename)
	if err != nil {
		//TODO: Handle this error better
		return nil, err
	}
	defer fileobj.Close()

	fileZ, err := gzip.NewReader(fileobj)
	if err != nil {
		//TODO: Handle this error better
		return nil, err
	}
	defer fileZ.Close()

	streamout, err := ioutil.ReadAll(fileZ)
	if err != nil {
		//TODO: Handle this error better
		return nil, err
	}
	return streamout, nil
}

// Read Gzip type log file
func GetObjectStreamGz(filename string, bucketName string) ([]byte, error) {
	// fmt.Println("Downlaoding: ", filename)

	resp, err := s3session.GetObject(&s3.GetObjectInput{
		Bucket: aws.String(bucketName),
		Key:    aws.String(filename),
	})

	if err != nil {
		return nil, err
	}

	// Compressed stream data
	bodyGz, errBody := gzip.NewReader(resp.Body)
	if errBody != nil {
		return nil, errBody
	}

	// Get stream data out into useable data
	bodyOut, errOut := ioutil.ReadAll(bodyGz)
	if errOut != nil {
		return nil, errOut
	}

	return bodyOut, nil
}

// alb String to Json
// string: 1 line entry of an ALB log
// r: regex compiled expression
// returns: string representation of a keyvalue json object of the alb line
func albStringToJson(s string, r *regexp.Regexp) string {
	values := r.FindStringSubmatch(s)
	keys := r.SubexpNames()

	d := make(map[string]string)
	for i := 1; i < len(keys); i++ {
		d[keys[i]] = values[i]
	}

	var logJsonLine strings.Builder
	fmt.Fprintf(&logJsonLine, "{ ")
	for logkey, logval := range d {
		fmt.Fprintf(&logJsonLine, "\"%s\":\"%s\", ", logkey, logval)
	}

	// Needs the last space and comma pulled out and final curly brace
	openJsonString := logJsonLine.String()

	// Take the slice, lop off last space and comma. Then close
	return openJsonString[:logJsonLine.Len()-2] + "}"
}

// Create a log stream to make things a bit cleaner
func CreateLogStream(logGroupName string, logStreamName string) (*cloudwatchlogs.CreateLogStreamOutput, error) {
	result, err := cloudlogger.CreateLogStream(&cloudwatchlogs.CreateLogStreamInput{
		LogGroupName:  aws.String(logGroupName),
		LogStreamName: aws.String(logStreamName),
	})

	fmt.Println("Stream created:", *result)
	return result, err
}

// Get Logstream description
func DescribeLogStreams(logGroupName string, logStreamName string) (*cloudwatchlogs.DescribeLogStreamsOutput, error) {

	result, error := cloudlogger.DescribeLogStreams(&cloudwatchlogs.DescribeLogStreamsInput{
		LogGroupName:        aws.String(logGroupName),
		LogStreamNamePrefix: aws.String(logStreamName),
	})
	if error != nil {
		return nil, error
	}

	return result, nil
}

// Write log event to cloud watch.
// longstreamId: string of log stream ID in cloud watch log group
// loggroupName: Name of log group in cloud waatch
// logToken: CloudWatch Sequence Token
// stringsonEvent: string of the json payload
// returns: PutEventsOutput, error. CWE PutEventsOutput from AWS and error string
func WriteLogStream(logstreamId string, loggroupName string, logToken string, eventBatch []*cloudwatchlogs.InputLogEvent) (*cloudwatchlogs.PutLogEventsOutput, error) {

	// Create PutLogEvents object. If there is not a next tokent, just send without it.
	var resultPutIntput cloudwatchlogs.PutLogEventsInput
	// fmt.Println("[DEBUG] Token input: ", logToken, "log group:", loggroupName, "stream name:", logstreamId)
	if logToken == "None" {
		// fmt.Println("[INFO] No token PutLog")
		resultPutIntput = cloudwatchlogs.PutLogEventsInput{
			LogEvents:     eventBatch,
			LogGroupName:  aws.String(loggroupName),
			LogStreamName: aws.String(logstreamId),
		}
	} else {
		// fmt.Println("[INFO] Next Token PutLog")
		resultPutIntput = cloudwatchlogs.PutLogEventsInput{
			LogEvents:     eventBatch,
			LogGroupName:  aws.String(loggroupName),
			LogStreamName: aws.String(logstreamId),
			SequenceToken: aws.String(logToken),
		}
	}

	// Push log events object. Basically just ship it as a batch
	result, errPutEvent := cloudlogger.PutLogEvents(&resultPutIntput)
	if errPutEvent != nil {
		return nil, errPutEvent
	}

	return result, nil
}

//

// Get the cloudwatch Sequence Token
// arguments: none, uses pointers of LOG_Group and LOG_STREAM
// returns:
//   string: Sequence Tokent
//   error: Error reported from getting the stream token.
func getSequenceToken(logGroupName string, logStreamName string) (string, error) {
	sequenceToken := "None"
	streamInfo, error := cloudlogger.DescribeLogStreams(&cloudwatchlogs.DescribeLogStreamsInput{
		LogGroupName:        aws.String(logGroupName),
		LogStreamNamePrefix: aws.String(logStreamName),
	})

	// fmt.Println("{\"logGroup\":\"", logGroupName, "\", \"logStream\": \"", logStreamName, "\" }")

	if error != nil {
		fmt.Println("[ERROR] Sequence Token Creation: ", error)
		return sequenceToken, error
	} else {
		if streamInfo.NextToken != nil {
			// fmt.Println("[INFO] Next Token:", *streamInfo.NextToken)
			sequenceToken = *streamInfo.NextToken
		} else {
			for _, stream := range streamInfo.LogStreams {
				// if stream.LogStreamName != nil {
				// 	fmt.Println("[INFO] Stream name:", *stream.LogStreamName)
				// }
				// if stream.Arn != nil {
				// 	fmt.Println("[INFO] Stream ARN", *stream.Arn)
				// }
				if stream.UploadSequenceToken != nil {
					// fmt.Println("[INFO] Uploadtoken Token:", *stream.UploadSequenceToken)
					sequenceToken = *stream.UploadSequenceToken
				}
			}
		}
	}
	// fmt.Println("Getting Sequence Token")
	return sequenceToken, nil
}

func mutexWriteLog(writeMutex *sync.RWMutex, nextToken *string, eventBatch []*cloudwatchlogs.InputLogEvent) {
	// Lock, anything from here on out is limited to control race conditions
	writeMutex.Lock()

	retry := false
	badToken := false

	logstreamId := *LOG_STREAM
	loggroupName := *LOG_GROUP

	if *nextToken == "None" {
		seqToken, errToken := getSequenceToken(*LOG_GROUP, *LOG_STREAM)
		if errToken != nil {
			fmt.Println("[ERROR] Cannot get next token in dataloop: ", errToken)
		}
		*nextToken = seqToken
	}

	// Create PutLogEvents object. If there is not a next tokent, just send without it.
	var resultPutIntput cloudwatchlogs.PutLogEventsInput
	// fmt.Println("[DEBUG] Token input: ", logToken, "log group:", loggroupName, "stream name:", logstreamId)
	if *nextToken == "None" {
		// fmt.Println("[INFO] No token PutLog")
		resultPutIntput = cloudwatchlogs.PutLogEventsInput{
			LogEvents:     eventBatch,
			LogGroupName:  aws.String(loggroupName),
			LogStreamName: aws.String(logstreamId),
		}
	} else {
		// fmt.Println("[INFO] Next Token PutLog")
		resultPutIntput = cloudwatchlogs.PutLogEventsInput{
			LogEvents:     eventBatch,
			LogGroupName:  aws.String(loggroupName),
			LogStreamName: aws.String(logstreamId),
			SequenceToken: aws.String(*nextToken),
		}
	}

	// Push log events object. Basically just ship it as a batch
	pushLog, errPutEvent := cloudlogger.PutLogEvents(&resultPutIntput)

	if errPutEvent != nil {
		retry = true
		fmt.Print("[Error] WriteLogger in loop handler:", errPutEvent)
		// If statement is only to report errors when writing the logs
		if pushLog.RejectedLogEventsInfo != nil {
			if pushLog.RejectedLogEventsInfo.ExpiredLogEventEndIndex != nil {
				fmt.Println("[ERROR] Expired Sequence:", *pushLog.RejectedLogEventsInfo.ExpiredLogEventEndIndex)
				if r := recover(); r != nil {
					fmt.Println("[ERROR] Recovered in ExpiredLogEvent")
				}
				badToken = true
			}
		} else {
			fmt.Println("[ERROR] No Next Sequence found. May not have sent logs")
		}
		if pushLog.RejectedLogEventsInfo.TooOldLogEventEndIndex != nil {
			fmt.Println("[ERROR] TooOld Sequence:", *pushLog.RejectedLogEventsInfo.TooOldLogEventEndIndex)
			badToken = true
		}
	} else {
		if pushLog.NextSequenceToken != nil {
			// Update pointer for next token
			*nextToken = *pushLog.NextSequenceToken
		}
	}

	if retry {
		if badToken {
			var errToken error
			*nextToken, errToken = getSequenceToken(*LOG_GROUP, *LOG_STREAM)
			if errToken != nil {
				fmt.Println("[ERROR] Issue getting second token")
			}
		}

		// fmt.Println("[INFO] Next Token PutLog")
		resultPutIntput = cloudwatchlogs.PutLogEventsInput{
			LogEvents:     eventBatch,
			LogGroupName:  aws.String(loggroupName),
			LogStreamName: aws.String(logstreamId),
			SequenceToken: aws.String(*nextToken),
		}

		// Push log events object. Basically just ship it as a batch
		pushLog2, errPutEvent2 := cloudlogger.PutLogEvents(&resultPutIntput)

		if errPutEvent2 != nil {
			fmt.Print("[Error] WriteLogger in loop 2 handler:", errPutEvent2)
			// If statement is only to report errors when writing the logs
			if pushLog2.RejectedLogEventsInfo != nil {
				if pushLog2.RejectedLogEventsInfo.ExpiredLogEventEndIndex != nil {
					fmt.Println("[ERROR] Expired 2 Sequence:", *pushLog2.RejectedLogEventsInfo.ExpiredLogEventEndIndex)
					if r := recover(); r != nil {
						fmt.Println("[ERROR] Recovered in ExpiredLogEvent 2")
					}
				} else {
					fmt.Println("[ERROR] No Next Sequence found 2. May not have sent logs")
				}
			} else {
				if pushLog2.NextSequenceToken != nil {
					// Update pointer for next token
					*nextToken = *pushLog2.NextSequenceToken
				}
			}
		}
	}

	// After this the resources are cleared.
	writeMutex.Unlock()
}

// Function for how to handle every s3 write event and designed to be called by a go routine.
//
func s3EventFunction(s3obj events.S3Entity, nextToken *string, writeMutex *sync.RWMutex, wg *sync.WaitGroup) {
	// On the return, notify waitgroup we're done.
	defer wg.Done()

	// Posts object information
	fmt.Printf("{\"type\": \"INFO\", \"Bucket\": \"%s\", \"Key\": \"%s\"}\n",
		s3obj.Bucket.Name, s3obj.Object.Key)

	// Call s3 bucket, download log, un-gzip log, and return file scanner object
	logData, errBucket := GetObjectStreamGz(s3obj.Object.Key, s3obj.Bucket.Name)
	if errBucket != nil {
		fmt.Print("[Error] GetObjectStreamGz in handler:", errBucket)
	}

	// Convert byte array to a scanner object
	logScanner := bufio.NewScanner(strings.NewReader(string(logData)))

	lineCount := 0
	var sizeCount uint64 = 0

	// Log events to add
	var cwePutInputs []*cloudwatchlogs.InputLogEvent
	for logScanner.Scan() { // Go through each unformatted line

		// First parse this line
		jsonLine := albStringToJson(logScanner.Text(), reALB) // Convert line to json key value

		// Calculate size of this json parsed event
		eventSize := uint64(len(jsonLine)) + BYTES_PER_EVENT

		// If at or above threshold, send batch to CloudWatch
		if sizeCount+eventSize >= MAX_BATCH_SIZE || len(cwePutInputs)+1 >= MAX_BATCH_EVENTS {

			// If too full from above, send and start buffer from 0
			fmt.Printf("{\"LineCount\": %d,\"EstimatedSize\": %d}\n", lineCount, sizeCount)

			// Call up mutex controlled log writter.
			mutexWriteLog(writeMutex, nextToken, cwePutInputs)

			// Reset counters and size after report
			sizeCount = 0
			lineCount = 0

			// cwePutInputs = nil // Clears out memory and capacity
			cwePutInputs = cwePutInputs[:0] // Clears contents, keeps allocated memory
		}

		cwe := cloudwatchlogs.InputLogEvent{
			Message:   &jsonLine,
			Timestamp: aws.Int64(time.Now().UnixNano() / int64(time.Millisecond)), // literally will not work without this
		}
		cwePutInputs = append(cwePutInputs, &cwe)

		// Increment count and size
		lineCount++
		sizeCount += eventSize
	}

	// Check buffer, if there is anything left, send it.
	if len(cwePutInputs) > 0 {

		// fmt.Println("{ \"EndLineCount\":\""+lineCount+"\"}")
		fmt.Printf("{\"EndLineCount\":\"%d\", \"EstimatedSize\":\"%d\"}\n", lineCount, sizeCount)

		// Call up mutex controlled log writter.
		mutexWriteLog(writeMutex, nextToken, cwePutInputs)
	}
}

// Lambda handler
func handler(ctx context.Context, s3Event events.S3Event) (string, error) {
	errCount := 0
	nextToken := "None"
	writeMutex := &sync.RWMutex{}
	var wg sync.WaitGroup

	// Read event hander message and s3 event. This code is from Amazon to help get started.
	for _, record := range s3Event.Records {
		s3obj := record.S3
		wg.Add(1)

		// Call event function in parallel seeing most of the time we're waiting for API calls.
		go s3EventFunction(s3obj, &nextToken, writeMutex, &wg)
	}

	// When all wait groups come back as done, close out. Bad things happen without this.
	wg.Wait()
	// Error stats, will show up in CloudWatch
	if 0 < errCount {
		return "Failure", fmt.Errorf("[ERROR] Error count: %d", errCount)
	}
	return "Success", nil
}

func main() {
	// Check and verify for Logstream first, get failures to show before handler is called.

	// // Create Log stream if necessary, keep if log stream should be created from Lambda
	// newStream, errNewStream := CreateLogStream(*LOG_GROUP, *LOG_STREAM)
	// if errNewStream != nil {
	// 	fmt.Println("Create stream failure:", errNewStream)
	// } else {
	// 	fmt.Println("New stream:", newStream)
	// }

	// Assuming everything is good, start and keep handler loaded.
	lambda.Start(handler)
}
